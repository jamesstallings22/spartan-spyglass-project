cmake_minimum_required (VERSION 3.5)

project (SrtPlugin CXX C)

set (CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "/usr/lib/llvm-5.0/lib/cmake/llvm")

find_package(LLVM REQUIRED)

add_definitions("-fno-rtti -std=c++11 ")
add_definitions("-D__STDC_LIMIT_MACROS -D__STDC_CONSTANT_MACROS ")

include(AddLLVM)

set(LLVM_ENABLE_WARNINGS ON)

include(HandleLLVMOptions)


add_definitions(${LLVM_DEFINITIONS})
include_directories(${LLVM_INCLUDE_DIRS})
link_directories(${LLVM_LIBRARY_DIRS})


add_llvm_loadable_module(SecondCheckerPlugin secondchecker.cpp)
