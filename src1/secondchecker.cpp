//=== secondchecker.cpp -----------------------------------*- C++ -*--===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//Copyright (c) <2018> <UNCG> 
//All rights reserved.
//Developed by: 		<LLVM>
//                  	<University of Illinois>
//                      <http://llvm.org>
//
//===----------------------------------------------------------------------===//
//
// This file defines secondchecker, which checks for unsafe integer Overflow
// check.
//
//===----------------------------------------------------------------------===//

#include<iostream>
#include "clang/StaticAnalyzer/Core/Checker.h"
#include "clang/StaticAnalyzer/Core/BugReporter/BugType.h"
#include "clang/StaticAnalyzer/Core/CheckerRegistry.h"
#include "clang/StaticAnalyzer/Core/PathSensitive/CheckerContext.h"


using namespace clang;
using namespace ento;

namespace {

  class secondchecker : public Checker<check::BranchCondition> {
    mutable std::unique_ptr<BugType> BT;


  public:
    void checkBranchCondition(const Stmt *Condition, CheckerContext &Ctx) const;
  };

}
void secondchecker::checkBranchCondition(const Stmt *Condition,
  CheckerContext &C) const {
    // SVal X = Ctx.getSVal(Condition);
    bool LHSisa2=false;
    bool RHSisa2=false;
    Expr *finalRHS=NULL;
    Expr *firstLHS=NULL;
    Expr *secondRHS=NULL;
    Expr *secondLHS=NULL;
    const BinaryOperator *BinOp1=NULL;
    const BinaryOperator *BinOp2=NULL;
    const DeclRefExpr *cond1=NULL;
    const DeclRefExpr *cond2=NULL;
    const DeclRefExpr *cond3=NULL;
    /*This if statement checks that the condition is a binary operator*/
    if(BinaryOperator::classof(Condition)){
      //This casts it to a binary operator and checks that the binary operator is a "<"
      if((BinOp1=dyn_cast<BinaryOperator>(Condition))){
        if(BinOp1->getOpcodeStr()=="<"){
          /*This fetches the left and right hand sides of the "<" */
          finalRHS= BinOp1->getRHS();
          firstLHS= BinOp1->getLHS();

        }
      }
    }
    //Here we check to make sure that both sides are not null
    if(finalRHS==NULL||firstLHS==NULL){
      return;
    }

    //We check the LHS of the condition to make sure it is a binary operator, then cast it and make sure it is a "*".
    if(BinaryOperator::classof(firstLHS)){
      if( ( BinOp2=dyn_cast<BinaryOperator>(firstLHS))){
        if(BinOp2->getOpcodeStr()=="*"){
          //Here we get the left and righthand side of the first LHS
          secondRHS= BinOp2->getRHS();
          secondLHS= BinOp2->getLHS();
        }
      }
    }
    if(secondRHS==NULL && secondLHS==NULL){
      return;
    }
    //Here we initialize the variables to a temporary version of them
    const Expr* tmpfinalRHS=NULL;
    tmpfinalRHS= finalRHS->IgnoreImpCasts();
    const Expr* tmpsecondRHS=NULL;
    const Expr* tmpsecondLHS=NULL;
    //After they are initialize them we chacek that their components are not null, then  we Ignore the implicit casts
    if(secondRHS!=NULL){
      tmpsecondRHS= secondRHS->IgnoreImpCasts();

    }
    if(secondLHS!=NULL){
      tmpsecondLHS= secondLHS->IgnoreImpCasts();
    }
    //Here we cast the variables after they have ignored the implicit casts
    if(tmpfinalRHS!=NULL && tmpsecondLHS!=NULL && tmpsecondRHS!=NULL){
      cond1=dyn_cast<DeclRefExpr>(tmpfinalRHS);
      cond2=dyn_cast<DeclRefExpr>(tmpsecondRHS);
      cond3=dyn_cast<DeclRefExpr>(tmpsecondLHS);
    }
    //Here we check that the First RHS casted to a DeclRefExpr isn't null and that  the tmpsecondLHS isn't null
    //Now we check if the Final/First RHS is the same as the secondLHS of the condition by looking at their declarations.
    //Then we throw the bug if it is true





    if(cond1!=NULL  && cond3!=NULL  && cond1->getDecl()==cond3->getDecl() ){
      //Bug thrown here
      ////This is the check for the 2*a<a condition
      SVal RHS1= C.getSVal(secondRHS);
        if(Optional<nonloc::ConcreteInt> RHS2 = RHS1.getAs<nonloc::ConcreteInt>()){
          if (RHS2->getValue()==2) {
            RHSisa2=true;

          }
        }

      if(!RHSisa2){
        if(!BT){
          BT.reset(new BugType(this, "Unsafe Integer Overflow Check", "example analyzer plugin"));
        }
        std::unique_ptr<BugReport> report =
        llvm::make_unique<BugReport>(*BT, BT->getName(), C.generateErrorNode());
        report->addRange(Condition->getSourceRange());
        C.emitReport(std::move(report));
      }
    }
    if(cond1!=NULL && cond2!=NULL && cond1->getDecl()==cond2->getDecl()){
      //Here we check that the First RHS casted to a DeclRefExpr isn't null and that  the tmpsecondRHS isn't null
      //Now we check if the Final/First RHS is the same as the secondRHS of the condition by looking at their declarations.
      //Then we throw the bug if it is true
      //This is the check for the 2*a<a condition
      SVal LHS1= C.getSVal(secondLHS);
        if(Optional<nonloc::ConcreteInt> LHS2 = LHS1.getAs<nonloc::ConcreteInt>()){
          if(LHS2->getValue()==2) {
            LHSisa2=true;
        }
       }
      if(!LHSisa2){
        if(!BT){
          BT.reset(new BugType(this, "Unsafe Integer Overflow Check", "example analyzer plugin"));
        }
        std::unique_ptr<BugReport> report =
        llvm::make_unique<BugReport>(*BT, BT->getName(), C.generateErrorNode());
        report->addRange(Condition->getSourceRange());
        C.emitReport(std::move(report));
      }
    }

  }


  extern "C"
  void clang_registerCheckers (CheckerRegistry &registry) {
    registry.addChecker<secondchecker>("example.secondchecker", "Looks for unsafe integer overflow error checks");
  }

  extern "C"
  const char clang_analyzerAPIVersionString[] = CLANG_ANALYZER_API_VERSION_STRING;
