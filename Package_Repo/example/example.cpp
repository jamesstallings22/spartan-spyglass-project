// Simple C++ program to display "Hello World"

// Header file for input output functions
#include<iostream>

using namespace std;

// main function -
// where the execution of program begins
int main()
{
  int a=3;
  int b=3;
  int c=1;
  int d=2;
  int e;

    // prints hello world
    //Uncomment the following if statement if you are checking to make sure you can ignore it
  if(2 *a<a){
  cout<<"Hello World";
  }

   //uncomment the following if statements if you want to make sure you can catch the bug
     //but only one at a time
     /*
     if(b *a<a){
     cout<<"Hello World";
     }
     */

     /*
     if(a*b<a){
     cout<<"Hello World";
     }
     */



    return 0;
}
