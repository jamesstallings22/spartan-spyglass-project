//=== firstchecker.cpp -----------------------------------*- C++ -*--===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file defines firstchecker, which dumps the AST context. 
//
//===----------------------------------------------------------------------===//


#include "clang/StaticAnalyzer/Core/Checker.h"
#include "clang/StaticAnalyzer/Core/BugReporter/BugType.h"
#include "clang/StaticAnalyzer/Core/CheckerRegistry.h"
#include "clang/StaticAnalyzer/Core/PathSensitive/CheckerContext.h"


using namespace clang;
using namespace ento;

namespace {

class firstchecker : public Checker<check::BranchCondition> {
  mutable std::unique_ptr<BuiltinBug> BT;


public:
  void checkBranchCondition(const Stmt *Condition, CheckerContext &Ctx) const;
};

}

void firstchecker::checkBranchCondition(const Stmt *Condition,
                                              CheckerContext &Ctx) const {
  // SVal X = Ctx.getSVal(Condition);
  Condition->dump();
}

extern "C"
void clang_registerCheckers (CheckerRegistry &registry) {
  registry.addChecker<firstchecker>("example.firstchecker", "dumps AST context of if statments");
}

extern "C"
const char clang_analyzerAPIVersionString[] = CLANG_ANALYZER_API_VERSION_STRING;
