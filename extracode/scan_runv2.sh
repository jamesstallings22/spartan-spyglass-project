cd $1
ls
 EXT=dsc
 for i in *; do
    if [ "${i}" != "${i%.${EXT}}" ]; then
      dpkg-source -x  $i
    fi
  done

 for dname in *; do
    if [ -d $dname ]; then
       cd $dname
       scan-build-5.0  -o ~/public_html/test4/$dname -load-plugin /storage/packagerepo/buildfolder/SecondCheckerPlugin.so  -enable-checker example.secondchecker dpkg-buildpackage -b
       cd ../
    fi
  cd ../
 done
