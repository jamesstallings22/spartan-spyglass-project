


#include <clang/StaticAnalyzer/Core/Checker.h>
#include <clang/StaticAnalyzer/Core/BugReporter/BugType.h>
#include <clang/StaticAnalyzer/Core/PathSensitive/CheckerContext.h>
#include <clang/ASTMatchers/ASTMatchers.h>
#include <clang/ASTMatchers/ASTMatchFinder.h>

using namespace clang;
using namespace clang::ast_matchers;
using namespace clang::ento;
using namespace llvm;


StatementMatcher ifMatcher =
  ifStmt(
          hasCondition(
            binaryOperator(
              hasOperatorName('<'), hasRHS().bind("finalRHS")
            )
          )
        hasCondition(
            binaryOperator(
              hasOperatorName('<'), hasLHS(BinaryOperator(hasOperatorName('*'), hasRHS().bind("firstRHS") ) ) )
            )
         hasCondition(
           binaryOperator(
             hasOperatorName('<'), hasLHS(BinaryOperator(hasOperatorName('*'), hasLHS().bind("firstLHS") ) ) )
           )
)

 class ifPrinter::(const MatchFinder::MatchResult & Result){
   ASTContext *Context= Result.Context;
   const IfStmt *IFS=Result.nodes.getStmtAs<IfStmt>("ifStatement");
   if(!IFS||!Context->getSourceManager().isFromMainFile(IFS->getIfLoc()))
   return;
   const VarDecl *FinalRHS =Result.Nodes.getNodeAs<VarDecl>("finalRHS");
   const VarDecl *FirstRHS =Result.Nodes.getNodeAs<VarDecl>("firstRHS");
   const VarDecl *FirstLHS =Result.Nodes.getNodeAs<VarDecl>("firstLHS");

   if(!areSameVariable(FinalRHS, FirstRHS)||!areSameVariable(FinalRHS, FirstLHS))
    return;
  llvm::outs() << "Potential Erroneous Integer Overflow check found.\n";
 }

 static bool areSameVariable(const ValueDecl *First, const ValueDecl *Second){
   return First && Second&&
    First->getCanonicalDecl() ==Second->getCanonicalDecl();
 }

int main(int argc, const **argv){
  CommonOptionsParser OptionsParser(argc, argv, MyToolCategory);
  ClangTool Tool(OptionsParser.getCompilations(),
                  OptionsParser.getSourcePathList());

  ifPrinter Printer;
  MatchFinder Finder;
  Finder.addMatcher(ifMatcher, &Printer);
  return Tool.run(newFrontendActionFactory(&Finder).get());

}
